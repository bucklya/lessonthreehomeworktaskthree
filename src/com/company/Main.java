package com.company;

import java.util.Scanner;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Программа предназначенна для вывода строк которые содержат в себе слово <<конь>> или <<арбуз>>. ");
        System.out.println("Введите сколько строк вы хотите проверить: ");
        Scanner numberOfStringsIn = new Scanner(System.in);
        int numberOfStrings = Integer.parseInt(numberOfStringsIn.nextLine());
        System.out.println("Введите " + numberOfStrings + " строк(и)");
        String[] arrayOfStrings = new String[numberOfStrings];

        for (int i = 0; i < numberOfStrings; i++) {
            Scanner stringIn = new Scanner(System.in);
            arrayOfStrings[i] = stringIn.nextLine();
        }

        String[] horseWatermelon = {"конь", "арбуз"};

        for (int i = 0; i < arrayOfStrings.length; i++) {
            if (arrayOfStrings[i].contains(horseWatermelon[0]) || arrayOfStrings[i].contains(horseWatermelon[1])) {
                System.out.println(arrayOfStrings[i]);
            }
        }
    }
}
